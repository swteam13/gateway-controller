import os

hostnames = [
    'www.google.com'
]

for hostname in hostnames:
    response = os.system('ping -c 1 ' + hostname)
    if response == 0:
        print (hostname, 'is up')
    else:
        print(hostname, 'is down')