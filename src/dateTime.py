import datetime
import sys

timeStamp = datetime.datetime.now()

print(timeStamp.strftime("%Y-%m-%d %H:%M:%S"))
sys.stdout.write(timeStamp.strftime("%Y-%m-%d %H:%M:%S"))