import os
import sys
psutilInstall = "pip install psutil"
os.system(psutilInstall)
import psutil


memoryFullCheck = psutil.disk_usage("/")
memoryUsed = psutil.disk_usage("/").used
memoryRemaining = psutil.disk_usage("/").free
memoryUsedPercent = psutil.disk_usage("/").percent


sys.stdout.write("Memory Used:", memoryUsed, "bytes  --  ", memoryUsedPercent, "%")
sys.stdout.write("Memory Remaining:", memoryRemaining, "bytes  --  ", 100-memoryUsedPercent, "%")


