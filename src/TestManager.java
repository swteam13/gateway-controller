import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestManager {

    public static String getDiagnostic(String diagnosticScript) {

        // for testing
        assert(diagnosticScript == "heartbeat.py" || diagnosticScript == "batteryTest.py"
                || diagnosticScript == "memoryTest.py" || diagnosticScript == "connectivityTest.py");

        String command = "";
        String prefix = "python";
        File testScriptFile = new File("./src/" + diagnosticScript);
        try {
            command = prefix + " " + testScriptFile.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("TEST FAIL");
        }
        Runtime rt = Runtime.getRuntime();
        Process proc = null;
        try {
            proc = rt.exec(command);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("TEST FAIL");
        }

        if (proc == null) {
            System.err.println("TEST FAIL");
            throw new RuntimeException("Could not start process for test " + diagnosticScript);
        }
        BufferedReader stdOut = new BufferedReader(new
                InputStreamReader(proc.getInputStream()));

        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(proc.getErrorStream()));

        // read the output from the command
        //System.out.println("STDOUT of " + diagnosticScript + ":\n");
        StringBuilder stdoutString = new StringBuilder();
        String s;
        bufferedReaderToStringBuilder(stdOut, stdoutString);
        //System.out.println(stdoutString + "\n");


        return String.valueOf(stdoutString);
    }

    private static void bufferedReaderToStringBuilder(BufferedReader stdOut, StringBuilder stdoutString) {
        String s;
        try {
            while ((s = stdOut.readLine()) != null) {
                stdoutString.append(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("TEST FAIL");
        }
    }
}
